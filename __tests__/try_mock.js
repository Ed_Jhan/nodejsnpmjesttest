import { Sample } from '../try_test_mock'

test('test 1', () => {
    const someMock = jest.fn();
    let sample = new Sample()
    sample.test1((a, b) => {
        expect(a).toBe(1);
        expect(b).toBe(2);
    });
});

test('test 2', () => {

    const mockCallback = jest.fn();

    let sample = new Sample()

    sample.test2(mockCallback)

    expect(mockCallback.mock.calls.length).toBe(2);

    expect(mockCallback.mock.calls[0][0]).toBe(3);
    expect(mockCallback.mock.calls[0][1]).toBe(4);
    expect(mockCallback.mock.calls[1][0]).toBe(5);
    expect(mockCallback.mock.calls[1][1]).toBe(6);
});

test('test 2_5', () => {


    let sample = new Sample()

    let obj = {
        func: (msg) => {
            expect(msg).toBe('hello')
        }

    }

    sample.test2_5(obj)
});

test('test 3', () => {
    let sample = new Sample()

    let res = {
        status: (num) => {
            expect(num).toBe(200)
            return res
        },
        json: (data) => {
            expect(data.urls[0]).toBe('url1')
            expect(data.urls[1]).toBe('rul2')
            return res
        },
        send: (weather) => {
            expect(weather).toBe('it is cool')
        }
    }

    sample.test3(res)
});

test('test 4', () => {
    let sut = new Sample()

    let factory = {
        create: (url, number) => {

            if (url === 'url1' && number === 'number') {
                return {
                    parse: () => [10, 20]
                }
            }
            else if (url === 'url1' && number === 'string') {
                return {
                    parse: () => ['10', '20']
                }
            }
            else if (url === 'url2' && number === 'xxx') {
                return {
                    parse: () => ['10', 20, true]
                }
            }
        }
    }

    let array = sut.test4(factory, 'url1', 'number')
    expect(array.length).toBe(2)
    expect(array[0]).toBe(10)
    expect(array[1]).toBe(20)

    array = sut.test4(factory, 'url1', 'string')
    expect(array.length).toBe(2)
    expect(array[0]).toBe('10')
    expect(array[1]).toBe('20')

    array = sut.test4(factory, 'url2', 'xxx')
    expect(array.length).toBe(3)
    expect(array[0]).toBe('10')
    expect(array[1]).toBe(20)
    expect(array[2]).toBeTruthy()

});

test('test 5', () => {

});