export class Sample {
    test1(callback) {
        callback(1, 2)
    }

    test2(callback) {
        callback(3, 4)
        callback(5, 6)
    }

    test2_5(obj) {
        obj.func('hello')
    }

    test3(res) {

        res.status(200).json({ urls: ["url1", "rul2"] }).send('it is cool')
    }

    test4(factory, url, body) {
        let parser = factory.create(url, body)
        return parser.parse()
    }

    test5(factory, res) {
        resultArray = []
        for (let url in ['url1', 'url2', 'url3']) {
            xmlArray = factory.create(url, 'body').parse()
            resultArray.push(xmlArray)
        }

        res.status(200).json({ response: resultArray })
    }
}